package com.dionetechnology.jira.plugins.notificationlistener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.ofbiz.core.entity.GenericEntityException;
import org.ofbiz.core.entity.GenericValue;

import com.atlassian.crowd.embedded.api.CrowdService;
import com.atlassian.crowd.embedded.api.User;
import com.atlassian.crowd.embedded.impl.ImmutableUser;
import com.atlassian.jira.ComponentManager;
import com.atlassian.jira.bc.user.UserService;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.event.issue.AbstractIssueEventListener;
import com.atlassian.jira.event.issue.IssueEvent;
import com.atlassian.jira.event.issue.IssueEventListener;
import com.atlassian.jira.event.issue.IssueEventSource;
import com.atlassian.jira.event.type.EventType;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.history.ChangeItemBean;
import com.atlassian.jira.mail.IssueMailQueueItemFactory;
import com.atlassian.jira.notification.AdhocNotificationService;
import com.atlassian.jira.notification.NotificationBuilder;
import com.atlassian.jira.notification.NotificationRecipient;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.user.UserPropertyManager;
import com.atlassian.mail.queue.MailQueueItem;
import com.atlassian.sal.api.component.ComponentLocator;
import com.atlassian.sal.api.user.UserManager;
import com.google.common.base.Optional;
import com.google.common.collect.ImmutableMap;
import com.opensymphony.util.TextUtils;



import static com.atlassian.jira.notification.AdhocNotificationService.ValiationOption.CONTINUE_ON_NO_RECIPIENTS;

/**
 * An event listener which triggers a workflow transition under certain conditions. For instance, this can be used to
 * trigger a workflow transition when a user comments or edits an issue in a particular state.
 */
public class NotificationListener extends AbstractIssueEventListener implements IssueEventListener
{
	private final static Logger log = Logger.getLogger(NotificationListener.class);

    // Don't change the text for these parameters, as they are used as parameter keys in the database, and the old
    // parameters will still be present and undeletable.

    protected static final String EVENT = "Event ID";
    protected static final String CUSTOMFIELD = "Text Custom Field";
    protected static final String PROJECT = "Project Key";

    
    private static final String EVENT_TRIGGER_EDIT = "edit";
    private static final String EVENT_TRIGGER_TRANSITION = "transition";
    private static final String EVENT_TRIGGER_ATTACHMENT_ADDED = "attachment_added";
    private static final String EVENT_TRIGGER_ATTACHMENT_DELETED = "attachment_deleted";
	private static final Set<String> POSSIBLE_EVENT_ACTIONS = new HashSet<String>(Arrays.asList(new String[]
			{EVENT_TRIGGER_EDIT, EVENT_TRIGGER_TRANSITION, EVENT_TRIGGER_ATTACHMENT_ADDED, EVENT_TRIGGER_ATTACHMENT_DELETED}));

    private Set<Long> eventIDs = Collections.emptySet();
    private Set<String> projectKeys = Collections.emptySet();
    private Set<String> eventTriggers = new HashSet<String>();

    private String userCustomField;
    private boolean allProjects = false;
    
    private AdhocNotificationService adhocNotificationService;

    @Override
	public String[] getAcceptedParams()
    {
        return new String[]{EVENT, PROJECT, CUSTOMFIELD};
    }

    @Override
	public String getDescription() {
        return "Transitions an issue given an event to listen for and an action to perform. Fields are as follows:"+
                "<ul style='{ li { margin-left: 10em } }'>"+
                "<li style='margin-top: 1em'><b>"+EVENT+"</b> - comma separated IDs of JIRA Events to listen to. Current possibilities are:<br><select multiple='multiple' size='5' disabled='true'>"+getEventsTable()+"</select>"+
                "<li style='margin-top: 1em'><b>"+CUSTOMFIELD+"</b> - Text Custom Field where the email adress will be located. <br><select multiple='multiple' size='15' disabled='true'>"+getCustomFields()+"</select>"+
                "<li style='margin-top: 1em'><b>"+PROJECT+"</b> - Only trigger for issues in these projects. This is a comma-separated list of keys. Possible keys are:<br><select multiple='multiple' size='3' disabled='true'>"+getProjectKeys()+"</select>"+
                "</ul>";
    }

    @Override
	public void init(Map params)
    {
    	
        adhocNotificationService = ComponentAccessor.getComponent(AdhocNotificationService.class);

        eventIDs  = new HashSet<Long>();
        if (params.containsKey(EVENT))
        {
        	String[] tokens = StringUtils.split((String) params.get(EVENT), ", \t;:");
        	for(String token: tokens)
			{
				if(StringUtils.isNumeric(token))
					eventIDs.add(Long.parseLong(token));
			}
        }

        
        projectKeys  = new HashSet<String>();
        if (params.containsKey(PROJECT))
        {
            String[] tokens = StringUtils.split(((String) params.get(PROJECT)), ", \t;:");
        	for(String token: tokens)
        	{
    			projectKeys.add(token.toLowerCase().trim());
        	}

        	allProjects = projectKeys.contains("*");
        }
        
        if (params.containsKey(CUSTOMFIELD)) {
        	userCustomField = (String) params.get(CUSTOMFIELD);
        }
        
        if (log.isInfoEnabled()) log.info(getInfoMessage());
    }

    private String getInfoMessage()
    {
    	StringBuffer buf = new StringBuffer("AutoTransitionListener configured to transition issues in ");
    	if(allProjects)
    		buf.append("all projects");
    	else if(projectKeys.isEmpty())
			buf.append("no projects (ie. disabled)");
    	else
    		buf.append("projects ");
    	buf.append(projectKeys);
        buf.append(" on events ");
        buf.append(eventIDs);
        return buf.toString();
    }

    @Override
	public void workflowEvent(IssueEvent event)
    {
        if(!eventIDs.contains(event.getEventTypeId().longValue()))
        {
        	if(log.isDebugEnabled())
        		log.debug("EventID '"+event.getEventTypeId().longValue()+"': is not one of "+eventIDs);
        	return;
        }

        Issue issue = event.getIssue();
        
        if(!isCorrectProject(issue))
        {
        	if(log.isDebugEnabled())
        		log.debug("EventID '"+event.getEventTypeId().longValue()+"': issue "+issue.getKey()+" in wrong project "+projectKeys);
        	return;
        }

        // get email address from customfield. 
        
        
        CustomField cf = getCustomField(userCustomField);
        String emailaddress = cf.getValueFromIssue(issue);
        if(log.isDebugEnabled())
    		log.debug("Email: "+emailaddress);
        
       
        adhocNotificationService.sendNotification(adhocNotificationService.validateNotification(makeNotificationBuilder(emailaddress, event), ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser(), issue, CONTINUE_ON_NO_RECIPIENTS));

  
    }
    
	private NotificationBuilder makeNotificationBuilder(String emailaddress, IssueEvent event)
    {
        final NotificationBuilder notificationBuilder = adhocNotificationService.makeBuilder();
		
		ArrayList<String> email = new ArrayList<String>();
		email.add(emailaddress);
		notificationBuilder.addToEmails(email);
		final ImmutableMap.Builder<String, Object> params = ImmutableMap.builder();
		
		log.error("eventtype:" + String.valueOf(event.getEventTypeId()));
		if(String.valueOf(event.getEventTypeId()).equals("1")) {
            //create
			notificationBuilder.setTemplate("issuecreated.vm");
		} else {
			notificationBuilder.setTemplate("issueupdated.vm");
			
			GenericValue changelog = event.getChangeLog(); 
	        params.put("changelog", changelog);
	        
		}
		notificationBuilder.setTemplateParams(params.build());
        return notificationBuilder;
    }



    /**
     * Parse the user's entry (if any) for a Cc user picker custom field.
     * @param customfieldId eg. "customfield_10000"
     * @return The specified custom field.
     */
    private CustomField getCustomField(String customfieldId)
    {
        if (customfieldId == null) return null;
        if ("false".equals(customfieldId)) return null;
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        CustomField field = customFieldManager.getCustomFieldObject(customfieldId);
        if (field == null) log.error("AutoTransitionListener configured to restrict transitions to custom field with id '" +
                customfieldId + "', but no such custom field was found. Value expected to be customfield_<id>, eg. customfield_10000.");
        return field;
    }



    private boolean isCorrectProject(Issue issue) {
        if(allProjects)
        	return true;
        return projectKeys.contains(issue.getProjectObject().getKey().toLowerCase());
    }



    //whether administrators can delete this listener
    @Override
	public boolean isInternal() {
        return false;
    }

    /**
     * Mail Listeners are unique.  It would be a rare case when you would want two emails sent out.
     */
    @Override
	public boolean isUnique() {
        return false;
    }

    private String getPossibleEventActions()
	{
    	StringBuffer buf = new StringBuffer();
    	for(String action : POSSIBLE_EVENT_ACTIONS)
		{
			buf.append("<li>");
			buf.append(action);
			buf.append("</li>");
		}
		return buf.toString();
	}

	private String getCustomFields()
    {
        StringBuffer buf = new StringBuffer();
        Collection<CustomField> custFields = ComponentAccessor.getCustomFieldManager().getCustomFieldObjects();
        for(CustomField cf: custFields)
        {
            buf.append("<option><b>").append(cf.getId()).append("</b> - ").append(TextUtils.htmlEncode(cf.getName())).append("</option>");
        }
        return buf.toString();
    }

    private String getProjectKeys() {
        StringBuffer buf = new StringBuffer();
        ProjectManager projectManager = ComponentAccessor.getProjectManager();
        Collection<Project> projects = projectManager.getProjectObjects();
        for(Project proj: projects)
        {
            buf.append("<option><b>").append(TextUtils.htmlEncode(proj.getKey())).append("</b></option>");
        }
        return buf.toString();

    }

    private String getEventsTable()
    {
        StringBuffer buf = new StringBuffer();
        Collection eventTypes = ComponentAccessor.getEventTypeManager().getEventTypes();
        Iterator iter = eventTypes.iterator();
        while (iter.hasNext()) {
            EventType e = (EventType) iter.next();
            buf.append("<option><b>").append(e.getId()).append("</b> - ").append(TextUtils.htmlEncode(e.getName())).append("</option>");
        }
        return buf.toString();
    }


}